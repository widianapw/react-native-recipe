import AsyncStorage from '@react-native-async-storage/async-storage';
import StorageKey from '../constants/StorageKey';
import getClient from '../services/getClient';
import {getErrorMessage} from './commonActions';

export const login = async loginData => {
  const {email, password} = loginData;
  try {
    const response = await getClient.post('login', {
      email: email,
      password: password,
    });
    const data = response.data;
    await AsyncStorage.setItem(StorageKey.ACCESS_TOKEN, data.data.access_token);
    return data;
  } catch (error) {
    getErrorMessage(error);
  }
};

export const register = async registerData => {
  const {name, phone, password, email, passwordConfirmation} = registerData;
  try {
    const response = await getClient.post('register', {
      name: name,
      phone: phone,
      password: password,
      email: email,
      password_confirmation: passwordConfirmation,
    });
    const data = response.data;
    await AsyncStorage.setItem(StorageKey.ACCESS_TOKEN, data.data.access_token);
  } catch (error) {
    getErrorMessage(error);
  }
};
