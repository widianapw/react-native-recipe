/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {createContext, useEffect, useMemo, useReducer} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppNavigation from './src/navigation/AppNavigation';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ThemeProvider} from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import StorageKey from './src/constants/StorageKey';

const theme = {
  colors: {
    primary: '#F3717F',
    colorPrimary: '#F3717F',
    primaryColor: '#F3717F',
  },
};

//auth
export const AuthContext = createContext();

// const Section = ({children, title}): Node => {
//   const isDarkMode = useColorScheme() === 'dark';
//   return (
//     <View style={styles.sectionContainer}>
//       <Text
//         style={[
//           styles.sectionTitle,
//           {
//             color: isDarkMode ? Colors.white : Colors.black,
//           },
//         ]}>
//         {title}
//       </Text>
//       <Text
//         style={[
//           styles.sectionDescription,
//           {
//             color: isDarkMode ? Colors.light : Colors.dark,
//           },
//         ]}>
//         {children}
//       </Text>
//     </View>
//   );
// };

const App = props => {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
        case 'LOADING_COMPLETE':
          return {
            ...prevState,
            isLoading: false,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    },
  );

  const authContextValue = React.useMemo(
    () => ({
      signIn: async data => {
        try {
          const uid = await AsyncStorage.getItem(StorageKey.ACCESS_TOKEN);
          dispatch({type: 'SIGN_IN', token: uid});
          console.log('State: ' + state.userToken);
        } catch (err) {
          console.log(err);
        }
      },
      signOut: async data => {
        dispatch({type: 'SIGN_OUT'});
      },
      doneLoading: async data => {
        dispatch({type: 'LOADING_COMPLETE'});
      },
    }),
    [],
  );

  React.useEffect(() => {
    // SplashScreen.hide();

    const bootstrapAsync = async () => {
      let userToken;

      try {
        userToken = await AsyncStorage.getItem(StorageKey.ACCESS_TOKEN);
        console.log('token: ' + userToken);
      } catch (e) {
        // Restoring token failed
        console.log(e);
      }

      dispatch({type: 'RESTORE_TOKEN', token: userToken});
    };

    bootstrapAsync();
  }, []);

  return (
    <SafeAreaProvider>
      <ThemeProvider theme={theme}>
        <AuthContext.Provider value={authContextValue}>
          <AppNavigation loggedIn={state.userToken} />
        </AuthContext.Provider>
      </ThemeProvider>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
