import React from 'react';
import {Image} from 'react-native';
import {View} from 'react-native';
import {Icon} from 'react-native-elements';
import CustomButton from '../atoms/CustomButton';
import CustomModal from '../atoms/CustomModal';
import CustomText from '../atoms/CustomText';
const RecipeModal = props => {
  const {selectedData, onClickDetail, isVisible, onTapOutside} = props;
  return (
    <CustomModal visibility={isVisible} onTapOutside={onTapOutside}>
      <View style={{width: '100%', height: 160}}>
        <View style={{flex: 1}}>
          <Image
            source={{
              uri: selectedData.imageUrl,
            }}
            style={{height: 160, width: '100%'}}
          />
          <View style={{position: 'absolute', right: 0}}>
            <Icon
              size={16}
              name="close"
              type="ionicon"
              raised
              onPress={onTapOutside}
            />
          </View>
        </View>
      </View>

      <View style={{padding: 20, width: '100%'}}>
        <CustomText body>{selectedData.description}</CustomText>
        <CustomButton title="Detail" onPress={onClickDetail} />
      </View>
    </CustomModal>
  );
};

export default RecipeModal;
