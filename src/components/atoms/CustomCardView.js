import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
const CustomCardView = props => {
  return (
    <TouchableOpacity
      {...props}
      activeOpacity={0.8}
      style={{...styles.card, ...props.style}}>
      {props.children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    marginVertical: 8,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.26,
    elevation: 3,
    backgroundColor: 'white',
    overflow: 'hidden',
    borderRadius: 10,
  },
});
export default CustomCardView;
