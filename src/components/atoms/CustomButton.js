import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Button} from 'react-native-elements';
import {ListItem} from 'react-native-elements/dist/list/ListItem';
import Colors from '../../constants/Colors';
const CustomButton = props => {
  const {color, title, textColor, disabled} = props;
  let buttonColor = color;
  buttonColor = disabled ? 'gray' : color;
  return (
    <View style={{marginVertical: 10}}>
      <TouchableOpacity activeOpacity={0.8} {...props}>
        <View
          style={{
            padding: 14,
            backgroundColor: buttonColor ? buttonColor : Colors.primary,
            borderRadius: 30,
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: textColor ? textColor : 'white',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  button: {
    borderRadius: 30,
    marginVertical: 10,
  },
});

export default CustomButton;
