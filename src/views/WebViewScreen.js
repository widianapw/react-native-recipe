import React from 'react';
import WebView from 'react-native-webview';
import CustomHeader from '../components/atoms/CustomHeader';
const WebViewScreen = props => {
  const {navigation, route} = props;
  const {url, title} = route.params;
  return (
    <>
      <CustomHeader headerTitle={title} navigation={navigation} />
      <WebView source={{uri: url}} />
    </>
  );
};

export default WebViewScreen;
