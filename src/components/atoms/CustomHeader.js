import React from 'react';
import {Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
const CustomHeader = props => {
  const {navigation, headerTitle} = props;
  return (
    <View
      style={{
        backgroundColor: 'white',
        height: 60,
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexDirection: 'row',
      }}>
      <View style={{padding: 10}}>
        <Icon
          name="arrow-back-outline"
          type="ionicon"
          onPress={() => navigation.goBack()}
        />
      </View>

      <Text style={{fontSize: 20, fontWeight: 'bold', marginStart: 10}}>
        {headerTitle ? headerTitle : 'RecipeAPP'}
      </Text>
    </View>
  );
};

export default CustomHeader;
