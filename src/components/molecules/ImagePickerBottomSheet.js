import React from 'react';
import {View} from 'react-native';
import Dash from 'react-native-dash';
import CustomButton from '../atoms/CustomButton';
import CustomOutlinedButton from '../atoms/CustomOutlinedButton';
import CustomText from '../atoms/CustomText';
const ImagePickerBottomSheet = props => {
  const {onClickGallery, onClickPicture} = props;
  return (
    <View
      style={{
        overflow: 'hidden',
        paddingTop: 20,
      }}>
      <View
        style={{
          backgroundColor: 'white',
          padding: 20,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: -10,
          },
          shadowOpacity: 0.1,
          shadowRadius: 5.0,
          elevation: 8,
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: 'gray',
            width: 50,
            alignSelf: 'center',
          }}></View>
        <View
          style={{
            marginVertical: 10,

            borderWidth: 1,
            borderColor: 'gray',
            width: 50,
            alignSelf: 'center',
          }}></View>
        <CustomOutlinedButton
          title="Take a Picture"
          icon="camera"
          type="outline"
          color="black"
          onPress={onClickPicture}
        />
        <CustomOutlinedButton
          icon="images"
          title="Choose from Gallery"
          type="outline"
          color="black"
          onPress={onClickGallery}
        />
      </View>
    </View>
  );
};

export default ImagePickerBottomSheet;
