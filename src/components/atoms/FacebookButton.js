import React from 'react';
import {Text} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {Image} from 'react-native';
import {View} from 'react-native';
const FacebookButton = props => {
  return (
    <View style={{marginVertical: 10}}>
      <TouchableOpacity activeOpacity={0.8} {...props}>
        <View
          style={{
            padding: 14,
            backgroundColor: 'blue',
            borderRadius: 30,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <View style={{paddingEnd: 10}}>
            <Image source={require('../../assets/images/facebook.png')} />
          </View>
          <Text
            style={{
              color: 'white',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Sign In with Facebook
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default FacebookButton;
