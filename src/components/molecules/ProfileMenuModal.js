import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Modal, View} from 'react-native';
import CustomText from '../atoms/CustomText';
const ProfileMenuModal = props => {
  const {
    isVisible,
    onTapEditProfile,
    onTapLogout,
    onTapOutside,
    isEditMode,
  } = props;
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={isVisible}
      onRequestClose={onTapOutside}>
      <TouchableOpacity
        activeOpacity={1}
        onPressOut={onTapOutside}
        style={{
          justifyContent: 'flex-start',
          flex: 1,
          alignItems: 'flex-end',
        }}>
        <View
          onPress={onTapOutside}
          style={{
            margin: 20,
            backgroundColor: 'white',
            borderRadius: 10,
            padding: 20,
            alignItems: 'center',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 4,
            elevation: 5,
          }}>
          <TouchableOpacity onPress={onTapEditProfile}>
            <CustomText body>
              {isEditMode ? 'View Profile' : 'Edit Profile'}
            </CustomText>
          </TouchableOpacity>
          <TouchableOpacity onPress={onTapLogout}>
            <CustomText body style={{color: 'red', marginTop: 16}}>
              Logout
            </CustomText>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </Modal>
  );
};

export default ProfileMenuModal;
