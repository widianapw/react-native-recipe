import React from 'react';
import {Dimensions} from 'react-native';
import {View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ShimmerPlaceholder from 'react-native-shimmer-placeholder';
const RecipeShimmer = props => {
  const {isDataLoaded} = props;
  return (
    <>
      <View style={{margin: 20, display: isDataLoaded ? 'none' : 'flex'}}>
        <ShimmerPlaceholder
          LinearGradient={LinearGradient}
          shimmerStyle={{
            height: 120,
            marginVertical: 8,
            borderRadius: 10,
            width: '100%',
          }}></ShimmerPlaceholder>
        <ShimmerPlaceholder
          LinearGradient={LinearGradient}
          shimmerStyle={{
            height: 120,
            marginVertical: 8,
            borderRadius: 10,
            width: '100%',
          }}></ShimmerPlaceholder>
        <ShimmerPlaceholder
          LinearGradient={LinearGradient}
          shimmerStyle={{
            height: 120,
            marginVertical: 8,
            borderRadius: 10,
            width: '100%',
          }}></ShimmerPlaceholder>
        <ShimmerPlaceholder
          LinearGradient={LinearGradient}
          shimmerStyle={{
            height: 120,
            marginVertical: 8,
            borderRadius: 10,
            width: '100%',
          }}></ShimmerPlaceholder>
        <ShimmerPlaceholder
          LinearGradient={LinearGradient}
          shimmerStyle={{
            height: 120,
            marginVertical: 8,
            borderRadius: 10,
            width: '100%',
          }}></ShimmerPlaceholder>
      </View>
    </>
  );
};

export default RecipeShimmer;
