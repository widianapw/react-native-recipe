import React, {
  useCallback,
  useContext,
  useEffect,
  useReducer,
  useRef,
  useState,
} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {showErrorAlert} from '../actions/commonActions';
import {
  getProfile,
  updateProfile,
  updateProfileImage,
} from '../actions/profileActions';
import CustomButton from '../components/atoms/CustomButton';
import CustomCenteredHeader from '../components/atoms/CustomCenteredHeader';
import CustomInput from '../components/atoms/CustomInput';
import BaseStyle from '../constants/BaseStyle';
import {Button} from 'react-native-elements';
import BottomSheet from 'reanimated-bottom-sheet';
import ImagePickerBottomSheet from '../components/molecules/ImagePickerBottomSheet';
import CustomModal from '../components/atoms/CustomModal';
import CustomText from '../components/atoms/CustomText';
import Animated from 'react-native-reanimated';
import {check, PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {BackHandler} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';
import ProfileMenuModal from '../components/molecules/ProfileMenuModal';
import {AuthContext} from '../../App';

const ProfileScreen = props => {
  const [profileData, setProfileData] = useState({});
  const {navigation} = props;
  const [showDiscardModal, setShowDiscardModal] = useState(false);
  const [isEditMode, setEditMode] = useState(false);
  const [isMenuShown, setMenuShown] = useState(false);
  const bottomSheetRef = useRef(null);
  const {signOut} = useContext(AuthContext);

  const fall = new Animated.Value(1);
  const animatedShadowOpacity = Animated.interpolateNode(fall, {
    inputRange: [0, 1],
    outputRange: [0.5, 0],
  });

  const showDiscardModalHandler = () => {
    setShowDiscardModal(!showDiscardModal);
  };

  const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
  const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
      const updatedValues = {
        ...state.values,
        [action.id]: action.value,
      };
      const updatedValidities = {
        ...state.validities,
        [action.id]: action.validity,
      };

      let updatedValidForm = true;
      for (const key in updatedValidities) {
        updatedValidForm = updatedValidForm && updatedValidities[key];
      }
      return {
        isFormValid: updatedValidForm,
        values: updatedValues,
        validities: updatedValidities,
      };
    }

    if (action.type === 'UPDATE_VALUES') {
      const updatedValues = {
        ...state.values,
        ['email']: action.email,
        ['phone']: action.phone,
        ['name']: action.name,
        ['image']: action.image,
      };
      return {
        values: updatedValues,
        isFormValid: true,
      };
    }
    return state;
  };

  const [formState, dispatchFormState] = useReducer(formReducer, {
    values: {
      name: profileData ? profileData.name : '',
      phone: profileData ? profileData.phone : '',
      email: profileData ? profileData.email : '',
      image: profileData ? profileData.image : '',
    },
    validities: {
      name: true,
      phone: true,
      email: true,
    },
    isFromValid: true,
  });

  const onInputChangeHandler = useCallback(
    (id, value, validity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        id: id,
        validity: validity,
        value: value,
      });
    },
    [dispatchFormState],
  );

  const changeProfileHandler = async () => {
    try {
      await updateProfile(formState.values);
      showErrorAlert({title: 'Success', message: 'Data updated successfully'});
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  const requestPermissionHandler = type => {
    let permission;
    switch (type) {
      case 'camera':
        permission = PERMISSIONS.ANDROID.CAMERA;
      default:
        permission = PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE;
    }
    check(permission).then(result => {
      if (result === RESULTS.GRANTED) {
        launchMedia(type);
      } else {
        request(permission).then(result => {
          if (result === RESULTS.GRANTED) {
            launchMedia(type);
          } else {
            showErrorAlert({
              message: 'Please accept the permission to use this feature',
            });
          }
        });
      }
    });
  };

  const launchMedia = type => {
    if (type === 'camera') {
      launchCamera(
        {
          mediaType: 'photo',
          includeBase64: false,
          quality: 0.2,
          saveToPhotos: true,
        },
        response => {
          if (response.errorMessage) {
            showErrorAlert({message: response.errorMessage});
          } else if (!response.didCancel) {
            updateUserImageHandler(response.uri);
          }
        },
      );
    } else {
      launchImageLibrary(
        {
          mediaType: 'photo',
          includeBase64: false,
          quality: 0.5,
        },
        response => {
          if (response.errorMessage) {
            showErrorAlert({message: response.errorMessage});
          } else if (!response.didCancel) {
            updateUserImageHandler(response.uri);
          }
        },
      );
    }
  };

  const updateUserImageHandler = async uri => {
    try {
      await updateProfileImage(uri);
      bottomSheetRef.current.snapTo(1);
      getProfileData();
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  const getProfileData = async () => {
    try {
      const response = await getProfile();
      const data = response.data;
      dispatchFormState({
        type: 'UPDATE_VALUES',
        email: data.email,
        phone: data.phone,
        name: data.name,
        image: data.image,
      });
      setProfileData(response.data);
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  useEffect(() => {
    getProfileData();
  }, [navigation]);

  useFocusEffect(
    useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', onBackPage);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPage);
    }),
  );

  const openImageResource = isOpen => {
    bottomSheetRef.current.snapTo(0);
  };

  const onBackPage = isToolbarBack => {
    if (isFormChanged()) {
      showDiscardModalHandler();
      return true;
    } else {
      if (isToolbarBack) {
        navigation.goBack();
      }
      return false;
    }
  };

  const isFormChanged = () => {
    let isChanged = false;
    if (profileData.email != formState.values.email) {
      isChanged = true;
    }
    if (profileData.phone != formState.values.phone) {
      isChanged = true;
    }
    if (profileData.name != formState.values.name) {
      isChanged = true;
    }
    return isChanged;
  };

  const renderBottomSheet = () => (
    <ImagePickerBottomSheet
      onClickPicture={() => {
        requestPermissionHandler('camera');
      }}
      onClickGallery={() => {
        // console.warn('GALLERY');
        requestPermissionHandler('gallery');
      }}
    />
  );
  return (
    <>
      <CustomCenteredHeader
        headerTitle="PROFILE"
        backable
        onBack={onBackPage.bind(this, true)}
        rightIcon="ellipsis-vertical"
        onTapRightIcon={() => {
          setMenuShown(!isMenuShown);
          bottomSheetRef.current.snapTo(1);
        }}></CustomCenteredHeader>
      <ScrollView contentContainerStyle={BaseStyle.scrollViewContainer}>
        <View
          style={{
            alignSelf: 'center',
            width: 160,
            height: 160,
            borderRadius: 80,
          }}>
          <Image
            source={
              formState.values.image
                ? {uri: formState.values.image}
                : require('../assets/images/user.png')
            }
            style={{
              width: 160,
              height: 160,
              borderRadius: 80,
            }}
          />
          {isEditMode && (
            <TouchableOpacity
              onPress={openImageResource.bind(this, true)}
              activeOpacity={0.8}
              style={{position: 'absolute', right: 0, bottom: 0, margin: 15}}>
              <Image source={require('../assets/images/plus_btn.png')} />
            </TouchableOpacity>
          )}
        </View>
        <View style={{width: '100%', marginTop: 30}}>
          <CustomInput
            id="name"
            label="Name"
            required
            editable={isEditMode}
            onInputChange={onInputChangeHandler}
            initialValue={formState.values.name}
          />
          <CustomInput
            id="phone"
            label="Phone"
            editable={isEditMode}
            required
            numeric
            keyboardType="number-pad"
            onInputChange={onInputChangeHandler}
            maximumLength={13}
            minimumLength={6}
            initialValue={formState.values.phone}
          />
          <CustomInput
            label="Email"
            id="email"
            required
            editable={isEditMode}
            email
            onInputChange={onInputChangeHandler}
            keyboardType="email-address"
            initialValue={formState.values.email}
          />
        </View>
        {isEditMode && (
          <View style={{justifyContent: 'flex-end', flex: 1}}>
            <CustomButton
              title="Change Profile"
              disabled={!formState.isFormValid}
              onPress={changeProfileHandler}
            />
          </View>
        )}
      </ScrollView>

      {/* bottomsheet background */}
      <Animated.View
        pointerEvents="none"
        style={[
          {
            ...StyleSheet.absoluteFillObject,
            backgroundColor: '#000',
            opacity: animatedShadowOpacity,
          },
        ]}></Animated.View>

      {/* image picker */}
      <BottomSheet
        ref={bottomSheetRef}
        initialSnap={1}
        borderRadius={10}
        snapPoints={[210, 0]}
        renderContent={renderBottomSheet}
        callbackNode={fall}
      />

      {/* Menu Modal */}
      <ProfileMenuModal
        isVisible={isMenuShown}
        onTapEditProfile={() => {
          setEditMode(!isEditMode);
          setMenuShown(!isMenuShown);
        }}
        isEditMode={isEditMode}
        onTapOutside={() => {
          setMenuShown(!isMenuShown);
        }}
        onTapLogout={() => {
          signOut();
        }}
      />

      {/* discard modal */}
      <CustomModal
        visibility={showDiscardModal}
        onTapOutside={showDiscardModalHandler}>
        <View style={{padding: 20, alignItems: 'center'}}>
          <Image source={require('../assets/images/alert.png')} />
          <CustomText style={{marginTop: 10}}>
            Are you sure want to exit?
          </CustomText>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              width: '100%',
              marginVertical: 10,
            }}>
            <Button
              title="Yes"
              type="clear"
              titleStyle={{color: 'black'}}
              onPress={() => {
                navigation.goBack();
                showDiscardModalHandler();
              }}
            />
            <Button
              title="No"
              type="clear"
              titleStyle={{color: 'black'}}
              onPress={showDiscardModalHandler}
            />
          </View>
        </View>
      </CustomModal>
    </>
  );
};

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    flex: 1,
    alignItems: 'center',
  },
});

export default ProfileScreen;
