import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useEffect, useLayoutEffect, useState} from 'react';
import {RefreshControl} from 'react-native';
import {FlatList, View} from 'react-native';
import {showErrorAlert} from '../actions/commonActions';
import {getFavoritesAPI} from '../actions/recipeActions';
import CustomCenteredHeader from '../components/atoms/CustomCenteredHeader';
import RecipeItem from '../components/molecules/RecipeItem';
import RecipeModal from '../components/molecules/RecipeModal';
import RecipeShimmer from '../components/shimmer/RecipeShimmer';
import BaseStyle from '../constants/BaseStyle';

const FavoriteScreen = props => {
  const {navigation} = props;
  const [data, setData] = useState([]);
  const [showDetailModal, setShowDetailModal] = useState(false);
  const [selectedData, setSelectedData] = useState({});
  const [refreshing, setRefreshing] = useState(false);
  const [isDataLoaded, setDataLoaded] = useState(false);
  const [page, setPage] = useState(1);
  let lastPage;

  const fetchData = async refetch => {
    try {
      refetch ? setDataLoaded(false) : null;
      const fetchPage = refetch ? 1 : page;
      const response = await getFavoritesAPI(fetchPage);
      if (fetchPage === 1) {
        setData(response.data);
      } else {
        setData([...data, ...response.data]);
      }
      setPage(fetchPage + 1);
      refetch ? setDataLoaded(true) : null;
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  const refreshHandler = () => {
    setRefreshing(true);
    fetchData(true);
    setRefreshing(false);
  };

  useFocusEffect(
    useCallback(() => {
      fetchData(true);
    }, [navigation]),
  );

  const showDetailModalHandler = () => {
    setShowDetailModal(!showDetailModal);
  };

  const renderItem = itemData => {
    const item = itemData.item;
    return (
      <RecipeItem
        item={item}
        onClick={() => {
          showDetailModalHandler();
          setSelectedData(item);
        }}
      />
    );
  };
  return (
    <>
      <CustomCenteredHeader
        backable
        onBack={() => navigation.goBack()}
        headerTitle="FAVORITE RECIPE"></CustomCenteredHeader>
      <View style={BaseStyle.screen}>
        <RecipeShimmer isDataLoaded={isDataLoaded} />
        <FlatList
          contentContainerStyle={{
            margin: 20,
            display: isDataLoaded ? 'flex' : 'none',
          }}
          data={data}
          refreshControl={
            <RefreshControl
              onRefresh={refreshHandler}
              refreshing={refreshing}
            />
          }
          keyExtractor={item => item.id}
          renderItem={renderItem}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            fetchData(false);
          }}
        />
      </View>

      {/* detail modal */}
      <RecipeModal
        selectedData={selectedData}
        isVisible={showDetailModal}
        onTapOutside={showDetailModalHandler}
        onClickDetail={() => {
          showDetailModalHandler();
          navigation.navigate('RecipeDetail', {
            id: selectedData.id,
          });
        }}
      />
    </>
  );
};

export default FavoriteScreen;
