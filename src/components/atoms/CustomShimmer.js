import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

import ShimmerPlaceholder, {
  createShimmerPlaceholder,
} from 'react-native-shimmer-placeholder';
const CustomShimmer = props => {
  const MShimmerPlaceholder = createShimmerPlaceholder(LinearGradient);
  return <MShimmerPlaceholder {...props}>{props.children}</MShimmerPlaceholder>;
};

export default CustomShimmer;
