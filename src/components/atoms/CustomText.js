import React from 'react';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native';
const CustomText = props => {
  const {h1, h2, h3, h4, body} = props;
  let fontSize = 14;
  let fontFamily = 'bold';
  if (h1) {
    fontSize = 22;
  } else if (h2) {
    fontSize = 20;
  } else if (h3) {
    fontSize = 18;
  } else if (h4) {
    fontSize = 16;
  } else if (body) {
    fontSize = 14;
    fontFamily = 'normal';
  }
  return (
    <Text
      style={{
        ...props.style,
        ...{fontWeight: fontFamily},
        ...{fontSize: fontSize},
      }}>
      {props.children}
    </Text>
  );
};

const localStyles = StyleSheet.create({
  text: {
    fontFamily: 'Rubik-Bold',
  },
});

export default CustomText;
