import React, {useEffect, useRef, useState} from 'react';
import {StyleSheet} from 'react-native';
import {Dimensions, ScrollView} from 'react-native';
import {View} from 'react-native';
import {Icon} from 'react-native-elements';
import Animated from 'react-native-reanimated';
import {showErrorAlert} from '../actions/commonActions';
import {
  getFavoritesAPI,
  getRecipeDetailAPI,
  setFavoriteRecipeAPI,
} from '../actions/recipeActions';
import CustomText from '../components/atoms/CustomText';
import BaseStyle from '../constants/BaseStyle';

const HEADER_EXPANDED_HEIGHT = 300;
const HEADER_COLLAPSED_HEIGHT = 60;
const {width: SCREEN_WIDTH} = Dimensions.get('screen');

const RecipeDetailScreen = props => {
  // const [scrollY, setasas] = useState(new Animated.Value(0));
  const {navigation, route} = props;
  const id = route.params.id;
  const [data, setData] = useState({});
  const [isFavorited, setFavorited] = useState(false);
  const fetchData = async () => {
    try {
      const response = await getRecipeDetailAPI(id);
      setData(response.data);
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  const fetchFavorites = async () => {
    try {
      const response = await getFavoritesAPI();
      const item = response.data.find(it => it.id === id);
      setFavorited(item != null);
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  const setFavoriteHandler = async () => {
    try {
      await setFavoriteRecipeAPI(id, !isFavorited);
      setFavorited(!isFavorited);
    } catch (error) {
      showErrorAlert({message: error.message});
    }
  };

  useEffect(() => {
    fetchData();
    fetchFavorites();
  }, [navigation]);

  let scrollY = useRef(new Animated.Value(0)).current;
  const headerHeight = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
    extrapolate: 'clamp',
  });
  const headerTitleOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });
  const recipeTitleOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  return (
    <>
      <View style={BaseStyle.screen}>
        <Animated.View style={[styles.header, {height: headerHeight}]}>
          <Animated.Image
            source={{uri: data.imageUrl}}
            style={[styles.image, {height: headerHeight}]}
          />
          <View style={{position: 'absolute', left: 0, margin: 8}}>
            <Icon
              size={16}
              name="arrow-back"
              type="ionicon"
              raised
              onPress={() => navigation.goBack()}
            />
          </View>
          <Animated.Text
            ellipsizeMode="tail"
            style={[styles.headerTitle, {opacity: headerTitleOpacity}]}>
            {data.name}
          </Animated.Text>
          <View style={styles.favoriteIconWrapper}>
            <Icon
              size={16}
              name={isFavorited ? 'heart' : 'heart-outline'}
              type="ionicon"
              raised
              color="red"
              onPress={setFavoriteHandler}
            />
          </View>
        </Animated.View>
        <Animated.ScrollView
          contentContainerStyle={styles.scrollContainer}
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {
                  y: scrollY,
                },
              },
            },
          ])}
          scrollEventThrottle={16}>
          <Animated.Text
            style={[styles.recipeTitle, {opacity: recipeTitleOpacity}]}>
            {data.name}
          </Animated.Text>
          <CustomText body style={{marginTop: 16}}>
            {data.description}
          </CustomText>
        </Animated.ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    padding: 16,
    paddingTop: HEADER_EXPANDED_HEIGHT,
  },
  header: {
    position: 'absolute',
    width: SCREEN_WIDTH,
    top: 0,
    left: 0,
    zIndex: 9999,
  },
  headerTitle: {
    position: 'absolute',
    borderColor: 'black',
    margin: 20,
    left: 50,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  recipeTitle: {
    marginTop: 20,
    fontWeight: 'bold',
    fontSize: 24,
  },
  image: {
    width: '100%',
  },
  favoriteIconWrapper: {
    position: 'absolute',
    right: 0,
    top: 0,
    margin: 8,
  },
});

export default RecipeDetailScreen;
