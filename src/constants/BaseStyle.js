import {StyleSheet} from 'react-native';

const BaseStyle = StyleSheet.create({
  scrollViewContainer: {
    flexGrow: 1,
    padding: 20,
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  screen: {
    flex: 1,
  },
});

export default BaseStyle;
