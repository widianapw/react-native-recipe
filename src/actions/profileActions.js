import AsyncStorage from '@react-native-async-storage/async-storage';
import StorageKey from '../constants/StorageKey';
import getClient from '../services/getClient';
import {getErrorMessage} from './commonActions';

export const getProfile = async () => {
  try {
    const response = await getClient.get('profile');
    const data = response.data;
    return data;
  } catch (error) {
    getErrorMessage(error);
  }
};

export const updateProfile = async profileData => {
  try {
    const response = await getClient.patch('profile', {
      name: profileData.name,
      email: profileData.email,
      phone: profileData.phone,
    });
    const data = response.data;
    return data;
  } catch (error) {
    getErrorMessage(error);
  }
};

export const updateProfileImage = async img => {
  try {
    const formData = createFormData(img);
    const response = await getClient.post('profile/image', formData);
    return response.data;
  } catch (error) {
    getErrorMessage(error);
  }
};

const createFormData = photo => {
  const data = new FormData();
  data.append('image', {
    name: 'photo.jpg',
    uri: Platform.OS === 'android' ? photo : photo.replace('file://', ''),
    type: 'image/jpeg',
  });
  return data;
};
