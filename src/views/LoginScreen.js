import React, {
  createContext,
  useCallback,
  useContext,
  useReducer,
  useState,
} from 'react';
import {Image, ScrollView, Text, TextInput, View} from 'react-native';
import Dash from 'react-native-dash';
import {Button, Input} from 'react-native-elements';
import CustomButton from '../components/atoms/CustomButton';
import CustomInput from '../components/atoms/CustomInput';
import CustomPasswordInput from '../components/atoms/CustomPasswordInput';
import BaseStyle from '../constants/BaseStyle';
import Modal from 'react-native-modal';
import CustomModal from '../components/atoms/CustomModal';
import {login} from '../actions/authActions';
import {showErrorAlert} from '../actions/commonActions';
import {AuthContext} from '../../App';
import CustomText from '../components/atoms/CustomText';
import CustomOutlinedButton from '../components/atoms/CustomOutlinedButton';
import FacebookButton from '../components/atoms/FacebookButton';
import GoogleButton from '../components/atoms/GoogleButton';
import CustomLine from '../components/atoms/CustomLine';

//formstate
const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.values,
      [action.id]: action.value,
    };
    const updatedValidities = {
      ...state.validities,
      [action.id]: action.validity,
    };
    let updatedValidForm = true;
    for (const key in updatedValidities) {
      updatedValidForm = updatedValidForm && updatedValidities[key];
    }
    return {
      isFormValid: updatedValidForm,
      values: updatedValues,
      validities: updatedValidities,
    };
  }
  return state;
};

const LoginScreen = props => {
  const {signIn} = useContext(AuthContext);
  const {navigation} = props;
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  //handler
  const showRegisterModalHandler = () => {
    setShowRegisterModal(!showRegisterModal);
  };

  const loginHandler = async () => {
    if (formState.isFormValid) {
      try {
        const response = await login(formState.values);
        signIn();
      } catch (error) {
        showErrorAlert({message: error.message});
      }
    } else {
      alert('not valid');
    }
  };

  const toRegisterPage = () => {
    navigation.navigate('Register');
  };

  //formstate
  const [formState, dispatchFormState] = useReducer(formReducer, {
    values: {
      email: '',
      password: '',
    },
    validities: {
      email: false,
      password: false,
    },
    isFormValid: false,
  });

  const onInputChangeHandler = useCallback(
    (id, value, validity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        id: id,
        validity: validity,
        value: value,
      });
    },
    [dispatchFormState],
  );

  return (
    <>
      <ScrollView contentContainerStyle={BaseStyle.scrollViewContainer}>
        <Image
          source={require('../assets/images/icon_logo.png')}
          style={{margin: 20}}
        />
        <CustomInput
          label="Email"
          id="email"
          email
          required
          keyboardType="email-address"
          onInputChange={onInputChangeHandler}
        />
        <CustomPasswordInput
          label="Password"
          id="password"
          onInputChange={onInputChangeHandler}
          required
          minimumLength={6}
        />
        <CustomButton
          title="LOGIN"
          disabled={!formState.isFormValid}
          onPress={loginHandler}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <Text>Don't have an account?</Text>
          <Button title="Register now" type="clear" onPress={toRegisterPage} />
        </View>
        <Dash
          style={{width: '100%', height: 0.5, marginVertical: 20}}
          dashColor="gray"
        />
        <CustomButton
          title="Sign in with another method"
          color="black"
          onPress={showRegisterModalHandler}
        />
      </ScrollView>

      {/* register with modal */}
      <CustomModal
        visibility={showRegisterModal}
        onTapOutside={showRegisterModalHandler}>
        <View style={{marginVertical: 20}}>
          <View style={{flexDirection: 'row'}}>
            <CustomLine />
            <CustomText h4 style={{margin: 4}}>
              Sign In Method
            </CustomText>
            <CustomLine />
          </View>
          <FacebookButton />
          <GoogleButton />
        </View>
      </CustomModal>
    </>
  );
};

export default LoginScreen;
