import React from 'react';
import Modal from 'react-native-modal';
import {View} from 'react-native';
const CustomModal = props => {
  const {visibility, onTapOutside} = props;
  return (
    <Modal
      isVisible={visibility}
      animationIn="fadeInLeft"
      animationOut="fadeOutRight"
      {...props}
      onBackButtonPress={onTapOutside}
      onBackdropPress={onTapOutside}>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View
          style={{
            margin: 20,
            backgroundColor: 'white',
            borderRadius: 20,
            alignItems: 'center',
            overflow: 'hidden',
          }}>
          {props.children}
        </View>
      </View>
    </Modal>
  );
};

export default CustomModal;
