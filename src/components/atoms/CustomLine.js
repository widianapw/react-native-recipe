import React from 'react';
import {View} from 'react-native';
const CustomLine = props => {
  return (
    <View
      style={{
        flex: 1,
        alignSelf: 'center',
        borderColor: 'black',
        width: 10,
        borderWidth: 1,
        height: 0.5,
      }}></View>
  );
};

export default CustomLine;
