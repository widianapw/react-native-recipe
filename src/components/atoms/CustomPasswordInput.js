import React, {useEffect, useReducer, useState} from 'react';
import {View, Text, TextInput} from 'react-native';
import {Icon} from 'react-native-elements';
const INPUT_CHANGE = 'INPUT_CHANGE';
const INPUT_BLUR = 'INPUT_BLUR';
const emailRe = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
const alphanumericRe = /^[a-z0-9]+$/i;

const inputReducer = (state, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {
        ...state,
        value: action.value,
        validity: action.validity,
        errorText: action.errorText,
        touched: true,
      };
    case INPUT_BLUR:
      return {
        ...state,
        touched: true,
      };
    default:
      return state;
  }
};

const capitalizeFirstLetter = text => {
  return text.charAt(0).toUpperCase() + text.slice(1);
};

const getErrorText = text => {
  let errorText = text.replace(/([a-z])([A-Z])/g, '$1 $2');
  errorText.replace(/([A-Z])([A-Z][a-z])/g, '$1 $2');
  return capitalizeFirstLetter(errorText.toLowerCase());
};

const CustomPasswordInput = props => {
  const {label} = props;
  const {id, onInputChange, counter, maxLength, initialValue} = props;
  const [inputState, dispatchInputState] = useReducer(inputReducer, {
    value: initialValue ? initialValue : '',
    validity: initialValue ? true : false,
    errorText: '',
  });

  const [showPassword, setShowPassword] = useState(false);

  const showPasswordHandler = () => {
    setShowPassword(!showPassword);
  };

  const onTextChangeHandler = text => {
    const {
      required,
      minimumLength,
      email,
      maximumLength,
      sameWith,
      sameWithId,
      alphanumeric,
    } = props;
    let textLength = text.length;
    let isValid = true;
    let errorText = '';

    if (minimumLength != null && textLength < minimumLength) {
      isValid = false;
      errorText = `${id} minimum length is ${minimumLength}`;
    }
    if (maximumLength != null && textLength > maximumLength) {
      isValid = false;
      errorText = `${id} maximum length is ${maximumLength}`;
    }
    if (email && !emailRe.test(text)) {
      isValid = false;
      errorText = 'Please enter valid email';
    }
    if (sameWith != null && text != sameWith) {
      isValid = false;
      errorText = `${id} should be same with ${sameWithId}`;
    }
    if (alphanumeric && !alphanumericRe.test(text)) {
      isValid = false;
      errorText = `${id} should be alphanumeric`;
    }
    if (required && textLength <= 0) {
      isValid = false;
      errorText = `${id} is required`;
    }
    errorText = getErrorText(errorText);
    dispatchInputState({
      type: INPUT_CHANGE,
      value: text,
      validity: isValid,
      errorText: errorText,
    });
  };

  useEffect(() => {
    if (inputState.touched) {
      onInputChange(id, inputState.value, inputState.validity);
    }
  }, [inputState, id, onInputChange]);
  return (
    <View style={{marginVertical: 10}}>
      <Text style={{marginStart: 16}}>{label}</Text>
      <View
        style={{
          borderRadius: 30,
          borderWidth: 1,
          borderColor:
            (!inputState.validity && !inputState.touched) || inputState.validity
              ? 'black'
              : 'red',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TextInput
          value={inputState.value}
          onChangeText={onTextChangeHandler}
          autoCapitalize="none"
          placeholder={label}
          secureTextEntry={!showPassword}
          style={{paddingHorizontal: 16, width: '87%'}}
          {...props}
        />
        <Icon
          name={showPassword ? 'eye-slash' : 'eye'}
          type="font-awesome"
          iconStyle={{}}
          onPress={showPasswordHandler}
        />
      </View>
      {!inputState.validity && inputState.touched && (
        <Text style={{color: 'red', marginStart: 16, fontSize: 12}}>
          {inputState.errorText}
        </Text>
      )}
    </View>
  );
};

export default CustomPasswordInput;
