import AsyncStorage from '@react-native-async-storage/async-storage';
import StorageKey from '../constants/StorageKey';
import getClient from '../services/getClient';
import {getErrorMessage} from './commonActions';

export const getRecipes = async page => {
  try {
    const response = await getClient.get('recipe', {
      params: {
        page: page,
      },
    });
    const data = response.data;
    return data;
  } catch (error) {
    getErrorMessage(error);
  }
};

export const getRecipeDetailAPI = async id => {
  try {
    const response = await getClient.get(`recipe/${id}`);
    return response.data;
  } catch (error) {
    getErrorMessage(error);
  }
};

export const setFavoriteRecipeAPI = async (id, toFavorite) => {
  try {
    let response;
    if (toFavorite) {
      response = await getClient.post(`favorite/${id}`);
    } else {
      response = await getClient.delete(`favorite/${id}`);
    }
    return response.data;
  } catch (error) {
    getErrorMessage(error);
  }
};

export const getFavoritesAPI = async page => {
  try {
    const response = await getClient.get('favorite', {
      params: {
        page: page,
      },
    });
    return response.data;
  } catch (error) {
    getErrorMessage(error);
  }
};
