import React, {
  createContext,
  useCallback,
  useContext,
  useReducer,
  useState,
} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {Button, CheckBox} from 'react-native-elements';
import {AuthContext} from '../../App';
import {register} from '../actions/authActions';
import {showErrorAlert} from '../actions/commonActions';
import CustomButton from '../components/atoms/CustomButton';
import CustomHeader from '../components/atoms/CustomHeader';
import CustomInput from '../components/atoms/CustomInput';
import CustomPasswordInput from '../components/atoms/CustomPasswordInput';
import BaseStyle from '../constants/BaseStyle';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const formReducer = (state, action) => {
  if (action.type === FORM_INPUT_UPDATE) {
    const updatedValues = {
      ...state.values,
      [action.id]: action.value,
    };
    const updatedValidities = {
      ...state.validities,
      [action.id]: action.validity,
    };

    let updatedValidForm = true;
    for (const key in updatedValidities) {
      updatedValidForm = updatedValidForm && updatedValidities[key];
    }
    return {
      isFormValid: updatedValidForm,
      values: updatedValues,
      validities: updatedValidities,
    };
  }
  return state;
};

const RegisterScreen = props => {
  const {signIn} = useContext(AuthContext);
  const {navigation} = props;
  const [checked, setChecked] = useState(false);
  const [formState, dispatchFormState] = useReducer(formReducer, {
    values: {
      name: '',
      phone: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      checked: false,
    },
    validities: {
      name: false,
      phone: false,
      email: false,
      password: false,
      passwordConfirmation: false,
      checked: false,
    },
    isFromValid: false,
  });

  const onInputChangeHandler = useCallback(
    (id, value, validity) => {
      dispatchFormState({
        type: FORM_INPUT_UPDATE,
        id: id,
        validity: validity,
        value: value,
      });
    },
    [dispatchFormState],
  );

  const registerHandler = async () => {
    if (formState.isFormValid) {
      try {
        const response = await register(formState.values);
        signIn();
      } catch (error) {
        showErrorAlert({message: error.message});
      }
    } else {
      alert('Not Valid');
    }
  };

  //navigations
  const toTermService = () => {
    navigation.navigate('WebView', {
      url: 'https://reactnative.dev',
      title: 'Terms and Service',
    });
  };

  const checkedHandler = () => {
    setChecked(!checked);
    onInputChangeHandler('checked', !checked, !checked);
  };

  return (
    <>
      <CustomHeader navigation={navigation} headerTitle="REGISTER" />
      <ScrollView contentContainerStyle={BaseStyle.scrollViewContainer}>
        <CustomInput
          label="Name"
          id="name"
          required
          onInputChange={onInputChangeHandler}
        />
        <CustomInput
          label="Phone"
          id="phone"
          required
          numeric
          keyboardType="number-pad"
          onInputChange={onInputChangeHandler}
          maximumLength={13}
          minimumLength={6}
        />
        <CustomInput
          label="Email"
          id="email"
          required
          email
          onInputChange={onInputChangeHandler}
          keyboardType="email-address"
        />
        <CustomPasswordInput
          label="Password"
          onInputChange={onInputChangeHandler}
          id="password"
          required
        />
        <CustomPasswordInput
          label="Re-Password"
          sameWith={formState.values.password}
          id="passwordConfirmation"
          required
          sameWithId="password"
          onInputChange={onInputChangeHandler}
        />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'center',
          }}>
          <CheckBox
            title="I have read the"
            checked={checked}
            onPress={checkedHandler}
            containerStyle={{
              backgroundColor: 'white',
              borderWidth: 0,
              marginEnd: -20,
            }}
          />
          <Button
            title="Terms and service"
            type="clear"
            onPress={toTermService}
          />
        </View>
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <CustomButton
            title="REGISTER"
            disabled={!formState.isFormValid}
            onPress={registerHandler}
          />
        </View>
      </ScrollView>
    </>
  );
};

export default RegisterScreen;
