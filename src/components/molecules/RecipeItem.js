import React from 'react';
import {ImageBackground, View} from 'react-native';
import CustomCardView from '../atoms/CustomCardView';
import CustomText from '../atoms/CustomText';
const RecipeItem = props => {
  const {item, onClick} = props;
  return (
    <CustomCardView onPress={onClick}>
      <ImageBackground
        source={{
          uri: item.imageUrl,
        }}
        style={{
          width: '100%',
          height: 120,
        }}>
        <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.5)'}}>
          <CustomText
            h2
            style={{
              position: 'absolute',
              bottom: 0,
              margin: 10,
              color: 'white',
              fontSize: 20,
            }}>
            {item.name}
          </CustomText>
        </View>
      </ImageBackground>
    </CustomCardView>
  );
};

export default RecipeItem;
