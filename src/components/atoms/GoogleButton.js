import React from 'react';
import {Image} from 'react-native';
import {Text} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {View} from 'react-native';
const GoogleButton = props => {
  return (
    <View style={{marginVertical: 10}}>
      <TouchableOpacity activeOpacity={0.5} {...props}>
        <View
          style={{
            padding: 14,
            backgroundColor: 'white',
            borderColor: 'black',
            borderWidth: 1,
            borderRadius: 30,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}>
          <View style={{paddingEnd: 10}}>
            <Image source={require('../../assets/images/ui.png')} />
          </View>
          <Text
            style={{
              color: 'black',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Sign In with Google
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default GoogleButton;
