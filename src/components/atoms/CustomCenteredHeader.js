import React from 'react';
import {Text} from 'react-native';
import {View} from 'react-native';
import {Button, Icon} from 'react-native-elements';
const CustomCenteredHeader = props => {
  const {
    headerTitle,
    buttonTitle,
    onClickButton,
    onBack,
    backable,
    rightIcon,
    onTapRightIcon,
  } = props;
  return (
    <View
      style={{
        backgroundColor: 'white',
        elevation: 5,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {backable && (
        <View style={{position: 'absolute', left: 10}}>
          <Icon name="arrow-back-outline" type="ionicon" onPress={onBack} />
        </View>
      )}
      <Text style={{fontSize: 20, fontWeight: 'bold'}}>
        {headerTitle ? headerTitle : 'RecipeAPP'}
      </Text>
      {rightIcon && (
        <View style={{position: 'absolute', right: 10}}>
          <Icon name={rightIcon} type="ionicon" onPress={onTapRightIcon} />
        </View>
      )}
      {buttonTitle && (
        <View style={{position: 'absolute', right: 10}}>
          <Button title={buttonTitle} type="clear" onPress={onClickButton} />
        </View>
      )}
    </View>
  );
};

export default CustomCenteredHeader;
