import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {FlatList, RefreshControl, View} from 'react-native';
import BaseStyle from '../constants/BaseStyle';
import CustomCenteredHeader from '../components/atoms/CustomCenteredHeader';
import * as commonActions from '../actions/commonActions';
import {getRecipes} from '../actions/recipeActions';
import BottomSheet from 'reanimated-bottom-sheet';
import MenuBottomSheet from '../components/molecules/MenuBottomSheet';
import {AuthContext} from '../../App';
import RecipeItem from '../components/molecules/RecipeItem';
import RecipeModal from '../components/molecules/RecipeModal';
import RecipeShimmer from '../components/shimmer/RecipeShimmer';
import {useFocusEffect} from '@react-navigation/core';

const RecipeScreen = props => {
  const {navigation} = props;
  const [recipeList, setRecipeList] = useState([]);
  const bottomSheetRef = useRef(null);
  const {signOut} = useContext(AuthContext);
  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const [selectedData, setSelectedData] = useState({});
  const [showDetailModal, setShowDetailModal] = useState(false);
  const [isDataLoaded, setDataLoaded] = useState(false);
  // let page = 1;

  const loadMoreData = () => {
    getRecipesHandler(page + 1);
  };

  const getRecipesHandler = async requestPage => {
    try {
      requestPage === 1 ? setDataLoaded(false) : null;
      const response = await getRecipes(requestPage);
      setPage(requestPage);
      requestPage == 1 ? setDataLoaded(true) : null;
      if (requestPage === 1) {
        setRecipeList(response.data);
      } else {
        setRecipeList([...recipeList, ...response.data]);
      }
    } catch (error) {
      commonActions.showErrorAlert({message: error.message});
    }
  };

  const getRecipeFirstTime = async () => {
    getRecipesHandler(1);
  };

  const refreshHandler = async () => {
    setRefreshing(true);
    await getRecipeFirstTime();
    setRefreshing(false);
  };

  const showDetailModalHandler = () => {
    setShowDetailModal(!showDetailModal);
  };

  useFocusEffect(
    useCallback(() => {
      bottomSheetRef.current.snapTo(1);
    }, [navigation]),
  );

  useEffect(() => {
    getRecipeFirstTime();
  }, [navigation]);

  // useEffect(() => {
  //   const resumeListener = navigation.addListener('willFocus', () => {

  //   })
  //   return navigation.
  // })

  const renderBottomSheet = () => (
    <MenuBottomSheet
      onClickProfile={() => {
        navigation.navigate('Profile');
      }}
      onClickLogout={() => {
        signOut();
      }}
      onClickFavorite={() => {
        navigation.navigate('Favorite');
      }}
    />
  );

  const renderItem = itemData => {
    const item = itemData.item;
    return (
      <RecipeItem
        item={item}
        onClick={() => {
          setSelectedData(item);
          showDetailModalHandler();
          bottomSheetRef.current.snapTo(1);
        }}
      />
    );
  };
  return (
    <>
      <CustomCenteredHeader headerTitle="RECIPES"></CustomCenteredHeader>
      <View style={BaseStyle.screen}>
        <RecipeShimmer isDataLoaded={isDataLoaded} />
        <FlatList
          contentContainerStyle={{
            margin: 20,
            paddingBottom: 60,
            display: isDataLoaded ? 'flex' : 'none',
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={refreshHandler}
            />
          }
          onEndReachedThreshold={0}
          onEndReached={({distanceFromEnd}) => {
            loadMoreData();
          }}
          data={recipeList}
          keyExtractor={item => item.id}
          renderItem={renderItem}
        />
      </View>
      <BottomSheet
        ref={bottomSheetRef}
        initialSnap={1}
        borderRadius={10}
        snapPoints={[350, 60]}
        renderContent={renderBottomSheet}
      />

      {/* Detail Modal */}
      <RecipeModal
        isVisible={showDetailModal}
        onTapOutside={showDetailModalHandler}
        selectedData={selectedData}
        onClickDetail={() => {
          showDetailModalHandler();
          navigation.navigate('RecipeDetail', {
            id: selectedData.id,
          });
        }}
      />
    </>
  );
};

export default RecipeScreen;
