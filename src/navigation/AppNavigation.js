import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import LoginScreen from '../views/LoginScreen';
import RegisterScreen from '../views/RegisterScreen';
import WebViewScreen from '../views/WebViewScreen';
import RecipeScreen from '../views/RecipeScreen';
import ProfileScreen from '../views/ProfileScreen';
import FavoriteScreen from '../views/FavoriteScreen';
import RecipeDetailScreen from '../views/RecipeDetailScreen';
const Stack = createStackNavigator();
const AppNavigation = props => {
  const {loggedIn} = props;
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          cardStyle: {
            backgroundColor: 'white',
          },
          headerShown: false,
        }}>
        {loggedIn != null ? (
          <>
            <Stack.Screen name="Recipe" component={RecipeScreen} />
            <Stack.Screen name="RecipeDetail" component={RecipeDetailScreen} />
            <Stack.Screen name="Profile" component={ProfileScreen} />
            <Stack.Screen name="Favorite" component={FavoriteScreen} />
          </>
        ) : (
          <>
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Register" component={RegisterScreen} />
            <Stack.Screen name="WebView" component={WebViewScreen} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
