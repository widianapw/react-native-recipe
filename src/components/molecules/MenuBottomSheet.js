import React from 'react';
import {View} from 'react-native';
import Dash from 'react-native-dash';
import CustomButton from '../atoms/CustomButton';
import CustomOutlinedButton from '../atoms/CustomOutlinedButton';
import CustomText from '../atoms/CustomText';
const MenuBottomSheet = props => {
  const {onClickProfile, onClickFavorite, onClickLogout} = props;
  return (
    <View
      style={{
        overflow: 'hidden',
        paddingTop: 20,
      }}>
      <View
        style={{
          backgroundColor: 'white',
          padding: 20,
          borderTopLeftRadius: 20,
          borderTopRightRadius: 20,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: -10,
          },
          shadowOpacity: 0.1,
          shadowRadius: 5.0,
          elevation: 8,
        }}>
        <View
          style={{
            borderWidth: 1,
            borderColor: 'gray',
            width: 50,
            alignSelf: 'center',
          }}></View>
        <View
          style={{
            marginVertical: 10,

            borderWidth: 1,
            borderColor: 'gray',
            width: 50,
            alignSelf: 'center',
          }}></View>
        <CustomText h2 style={{alignSelf: 'center'}}>
          MENU
        </CustomText>
        <Dash
          style={{width: '100%', height: 0.5, marginVertical: 20}}
          dashColor="gray"
        />

        <CustomOutlinedButton
          title="PROFILE"
          type="outline"
          color="black"
          onPress={onClickProfile}
        />
        <CustomOutlinedButton
          title="FAVORITE"
          type="outline"
          color="black"
          onPress={onClickFavorite}
        />
        <CustomButton
          title="LOGOUT"
          type="outline"
          color="red"
          onPress={onClickLogout}
        />
      </View>
    </View>
  );
};

export default MenuBottomSheet;
