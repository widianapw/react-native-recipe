import React from 'react';
import {Image} from 'react-native';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import Colors from '../../constants/Colors';
const CustomOutlinedButton = props => {
  const {color, title, textColor, disabled, icon} = props;
  let buttonColor = color;
  buttonColor = disabled ? 'gray' : color;
  return (
    <>
      <View style={{marginVertical: 10}}>
        <TouchableOpacity activeOpacity={0.5} {...props}>
          <View
            style={{
              padding: 14,
              backgroundColor: 'white',
              borderColor: buttonColor ? buttonColor : Colors.primary,
              borderWidth: 1,
              borderRadius: 30,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {icon && (
              <View style={{position: 'absolute', left: 15}}>
                <Icon name={icon} type="ionicon" />
              </View>
            )}
            <Text
              style={{
                color: textColor ? textColor : 'black',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              {title}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </>
  );
};
const style = StyleSheet.create({
  button: {
    borderRadius: 30,
    marginVertical: 10,
  },
});

export default CustomOutlinedButton;
